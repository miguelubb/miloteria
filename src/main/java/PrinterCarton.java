import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.DottedLineSeparator;

import java.io.FileOutputStream;
import java.util.Arrays;

public class PrinterCarton {
    private static Font fontTit = FontFactory.getFont(FontFactory.HELVETICA_BOLD, 20, BaseColor.BLACK);
    private static Font font = FontFactory.getFont(FontFactory.HELVETICA, 18, BaseColor.BLACK);
    private static Font font2 = FontFactory.getFont(FontFactory.HELVETICA, 14, BaseColor.BLACK);
    private static  Font font3 = FontFactory.getFont(FontFactory.HELVETICA, 11, BaseColor.BLACK);

    public static void print(Carton c) throws Exception {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("carton_" + c.getSerie() + ".pdf"));
        Rectangle one = new Rectangle(559, 240);
        //document.setPageSize(one);
        document.setMargins(2, 1, 1, 2);

        document.open();

       /* PdfContentByte canvas = writer.getDirectContent();
        Rectangle rect = new Rectangle(0, 0, 559, 240);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(2);
        canvas.rectangle(rect); */
        Chunk titulo = new Chunk("\tSorteo Millonario\n", fontTit);
        Chunk subTitulo = new Chunk("\tA beneficio del 8°D CPH, pro-gira de estudios\n\n", font2);
        Chunk serie = new Chunk("\tCartón: " + c.getSerie() + "\n", font);
        Chunk numeros = new Chunk("\tNúmeros: " + Arrays.toString(c.getNum()) + "\n", font);
        Chunk nombre = new Chunk("\tPropietario: " + c.getPropietario() + "\n", font);
        Chunk valor = new Chunk("\tValor: $1.000.-\n\n", font);
        Chunk id = new Chunk(c.getId(), font3);
        Image img = Image.getInstance(c.getQR());
        Paragraph first = new Paragraph();
        first.setIndentationLeft(20);
        first.setSpacingAfter(3.0f);
        first.add(titulo);
        first.add(subTitulo);
        first.add(serie);
        first.add(numeros);
        first.add(nombre);
        first.add(valor);

        //first.add(img);
        first.add(id);

        PdfPTable table = new PdfPTable(2);
        table.setWidths(new int[]{10, 4});
        table.addCell(first);
        table.addCell(new PdfPCell(img, true));


        document.add(table);

        document.close();
    }

    public static void print(Carton[] cartones) throws Exception {
        print(cartones,"cartonesSorteoMillonario.pdf");
    }

    public static void print(Carton[] cartones, String filename) throws Exception {
        Document document = new Document();
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));

        //Rectangle one = new Rectangle(559, 240);
        //document.setPageSize(one);
        //TAMAÑO LETTER: 612.0F, 792.0F
        //document.setPageSize(PageSize.LETTER);
        document.setPageSize(PageSize.A4);

        document.open();
        document.setMargins(30f, 30f, 30f, 30f);


       /*
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        PdfContentByte canvas = writer.getDirectContent();
        Rectangle rect = new Rectangle(0, 0, 559, 240);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(2);
        canvas.rectangle(rect); */

        for (int i = 0; i < cartones.length; i++) {
            Carton c = cartones[i];
            Chunk titulo = new Chunk("Sorteo Millonario\n", fontTit);
            Chunk subTitulo = new Chunk("A beneficio del 8°D CPH, pro-gira de estudios\n\n", font2);
            Chunk serie = new Chunk("Cartón: " + c.getSerie() + "\n", font);
            Chunk numeros = new Chunk("Números: " + Arrays.toString(c.getNum()) + "\n", font);
            Chunk nombre = new Chunk("Propietario: " + c.getPropietario() + "\n", font);
            Chunk valor = new Chunk("\nValor: $1.000.-\n\n", font);
            Chunk id = new Chunk("hash: "+c.getId(), font3);
            Image img = Image.getInstance(c.getQR());
            Paragraph first = new Paragraph();
            Paragraph second=new Paragraph();

            first.setIndentationLeft(20);
            //first.setSpacingAfter(8.0f);

            first.add(titulo);
            first.add(subTitulo);
            first.add(serie);
            first.add(numeros);
            first.add(nombre);
            first.add(valor);
            second.add(id);

            PdfPTable table = new PdfPTable(3);
            table.setWidths(new int[]{1,10, 5});

            table.addCell("");
            table.addCell(first);
            table.addCell(new PdfPCell(img, true));
            PdfPCell pdfPCell=new PdfPCell(new Phrase(id));
            pdfPCell.setColspan(3);
            pdfPCell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            pdfPCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);

            table.addCell(pdfPCell);
            table.setWidthPercentage(100);//fundamental para ampliar la tabla
            table.setSpacingAfter(10f);

            document.add(table);
            /*DottedLineSeparator dottedline = new DottedLineSeparator();
            dottedline.setOffset(-2);
            dottedline.setGap(2f);
            document.add(dottedline);*/
        }

        document.close();
    }
}
