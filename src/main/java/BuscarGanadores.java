import javax.swing.*;
import java.awt.event.*;

public class BuscarGanadores extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField n1;
    private JTextField n2;
    private JTextField n3;
    private JTextField n4;
    private JTextField n5;
    private JTextField ganador5;
    private JTextField ganador4;

    public BuscarGanadores() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        byte[] ganadores=Sorteo.getInstance().getNumGanadores();
        n1.setText(""+ganadores[0]);
        n2.setText(""+ganadores[1]);
        n3.setText(""+ganadores[2]);
        n4.setText(""+ganadores[3]);
        n5.setText(""+ganadores[4]);
        ganador5.setText(Sorteo.getInstance().ganador5());
        ganador4.setText(Sorteo.getInstance().ganador4());

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }


    private void onOK() {
        // add your code here
        byte[] numGanadores = new byte[5];
        numGanadores[0]=Byte.parseByte(n1.getText());
        numGanadores[1]=Byte.parseByte(n2.getText());
        numGanadores[2]=Byte.parseByte(n3.getText());
        numGanadores[3]=Byte.parseByte(n4.getText());
        numGanadores[4]=Byte.parseByte(n5.getText());
        Sorteo.getInstance().setNumGanadores(numGanadores);
        Sorteo.getInstance().setEstado(Sorteo.SORTEO_CERRADO);
        ganador5.setText(Sorteo.getInstance().ganador5());
        ganador4.setText(Sorteo.getInstance().ganador4());

    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        BuscarGanadores dialog = new BuscarGanadores();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
    public static void showDialog() {
        BuscarGanadores dialog = new BuscarGanadores();
        dialog.pack();
        dialog.setVisible(true);
    }

/*
    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
    */
}
