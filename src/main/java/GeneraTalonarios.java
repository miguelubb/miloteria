import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;

public class GeneraTalonarios extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JRadioButton alPortadorRadioButton;
    private JRadioButton nominativosRadioButton;
    private JTextField nombre;
    private JTextField archivo;
    private JSpinner cantidad;
    private JLabel folios;

    public GeneraTalonarios() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);


        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        alPortadorRadioButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                nombre.setText("Al portador");
                nombre.setEnabled(false);
            }
        });
        nominativosRadioButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                nombre.setText("");
                nombre.setEnabled(true);
            }
        });

        cantidad.setValue(new Integer(1));
        onChangeCantidad();

        JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor)cantidad.getEditor();
        JTextField textField = editor.getTextField();
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                onChangeCantidad();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                onChangeCantidad();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                onChangeCantidad();
            }
        });


        nombre.addFocusListener(new FocusAdapter() {
            /**
             * Invoked when a component loses the keyboard focus.
             *
             * @param e
             */
            @Override
            public void focusLost(FocusEvent e) {
                super.focusLost(e);
                archivo.setText(toFileName(nombre.getText()));
            }
        });
        nombre.addPropertyChangeListener(new PropertyChangeListener() {
            /**
             * This method gets called when a bound property is changed.
             *
             * @param evt A PropertyChangeEvent object describing the event source
             *            and the property that has changed.
             */
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                archivo.setText(toFileName(nombre.getText()));
            }
        });


    }

    private void onChangeCantidad() {
        int cant=(Integer) cantidad.getValue();
        int desde=Sorteo.getInstance().getFolioActual();
        int hasta=desde + cant-1;
        folios.setText("de "+desde+" al "+hasta);
    }

    private void onOK() {
        // add your code here
        //validar
        //nombre no null
        if(nombre.getText().equals("")){
            JOptionPane.showMessageDialog(this,"ingrese nombre");
            return;
        }
        if(0>=(int)cantidad.getValue()){
            JOptionPane.showMessageDialog(this,"la cantidad debe ser mayor a 0");
            return;
        }
        if(archivo.getText().equals("")){
            JOptionPane.showMessageDialog(this,"ingrese nombre de archivo");
            return;
        }
        try {
            cantidad.commitEdit();
        } catch ( java.text.ParseException e ) {
            System.out.println("Error en el commitEdit");//ignoro el error
        }

        //poner el cursor como reloj de arena
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        int cant=(Integer) cantidad.getValue();
        Carton[] cartones=new Carton[cant];
        for(int i=0;i<cartones.length;i++){
            cartones[i]=new Carton(nombre.getText());
        }


        try {
            PrinterCarton.print(cartones,archivo.getText());
            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

            Sorteo.getInstance().addCartones(cartones);
            Sorteo.getInstance().toFile("Sorteo.obj");//todo:usar constante inyectada
            JOptionPane.showMessageDialog(this,"Cartones generados satisfactoriamente");
            onChangeCantidad();
        } catch (Exception e) {
            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            JOptionPane.showMessageDialog(this,"No se pueden guardar los cartones. Verifique el nombre del archivo");
        }
    }
    private String toFileName(String s){
        String out="";
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)==' '){
                out+="_";
            }else{
                out+=s.charAt(i);
            }
        }
        return out+".pdf";
    }
    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        try {
            Sorteo.getInstance().fromFile("Sorteo.obj");
        } catch (IOException e) {
            //si no se puede leer es porque no existe y no hay problemas si es la primera vez.
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        showDialog();
        System.exit(0);
    }

    public static void showDialog() {

        GeneraTalonarios dialog = new GeneraTalonarios();
        dialog.pack();
        dialog.setVisible(true);

    }
}
