import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class ScanQRDialog extends JDialog {
    private Executor executor = Executors.newSingleThreadExecutor();
    private AtomicBoolean initialized = new AtomicBoolean(false);
    private Webcam webcam = null;
    private WebcamPanel panel = null;
    private String msg=null;
    public ScanQRDialog() {
       setModal(true);
        webcam = Webcam.getDefault();
        webcam.setViewSize(webcam.getViewSizes()[0]);
        panel = new WebcamPanel(webcam, false);
        panel.setPreferredSize(webcam.getViewSize());
        panel.setOpaque(true);
        panel.setBackground(Color.BLACK);
        panel.setBounds(0, 0, 400, 300);

        getContentPane().add(panel);
        if (initialized.compareAndSet(false, true)) {
            executor.execute(new Runnable() {

                @Override
                public void run() {
                    panel.start();
                    webcam.open();

                }
            });
        }

        addKeyListener(new KeyAdapter() {
            /**
             * Invoked when a key has been pressed.
             *
             * @param e
             */
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                scanAndCloseWebcam();
            }
        });
    }

    private void scanAndCloseWebcam() {
        msg=null;
        do {
            BufferedImage img = webcam.getImage();
            msg = QRGenerator.decodeQRCode(img);
        }while (msg==null);
        webcam.close();
        dispose();
    }


    public static String showDialog() {
        ScanQRDialog dialog=new ScanQRDialog();
        dialog.pack();
        dialog.setVisible(true);
        return dialog.msg;
    }



    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
