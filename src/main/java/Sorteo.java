import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Sorteo implements Serializable {
    private int folioActual=10000;
    private ArrayList<Carton> cartonesGenerados=new ArrayList<>();
    private byte[] numGanadores=new byte[5];
    private int estado=SORTEO_ABIERTO;
    public static int SORTEO_ABIERTO=1;
    public static int SORTEO_CERRADO=2;



    //implementación de singleton

    private static Sorteo INSTANCIA;

    private Sorteo() {

    }
    public static Sorteo getInstance(){
        if(INSTANCIA==null){
            INSTANCIA=new Sorteo();
        }
        return INSTANCIA;
    }

    public int getFolioActual() {
        return folioActual;
    }

    public void setFolioActual(int folioActual) {
        this.folioActual = folioActual;
    }

    public ArrayList<Carton> getCartonesGenerados() {
        return cartonesGenerados;
    }

    public void setCartonesGenerados(ArrayList<Carton> cartonesGenerados) {
        this.cartonesGenerados = cartonesGenerados;
    }

    public byte[] getNumGanadores() {
        return numGanadores;
    }

    public void setNumGanadores(byte[] numGanadores) {
        Arrays.sort(numGanadores);
        this.numGanadores = numGanadores;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public int generarSerie(){
        return folioActual++;
    }

    public void toFile(String filename) throws IOException {
        ObjectOutputStream out=new ObjectOutputStream(new FileOutputStream(filename));
        out.writeInt(folioActual);
        out.writeObject(cartonesGenerados);
        out.write(numGanadores);
        out.writeInt(estado);
        out.flush();
        out.close();
    }
    public void fromFile(String filename) throws IOException, ClassNotFoundException {
        ObjectInputStream in=new ObjectInputStream(new FileInputStream(filename));
        folioActual=in.readInt();
        cartonesGenerados=(ArrayList<Carton>)in.readObject();
        in.readFully(numGanadores);
        estado=in.readInt();
        in.close();
    }

    public void addCartones(Carton[] cartones) {
        for(Carton c:cartones) {
            cartonesGenerados.add(c);
        }
    }

    public String ganador5(){
        String ganadores="";
        for(Carton c:cartonesGenerados){
            if(Arrays.equals(c.getNum(),numGanadores)){
                ganadores+=c.getSerie()+" | ";
            }
        }
        return ganadores;
    }

    public String ganador4(){
        String ganadores="";
        for(Carton c:cartonesGenerados){
            if(acierta4(c.getNum(),numGanadores)){
                ganadores+=c.getSerie()+" | ";
            }
        }
        return ganadores;
    }


    public boolean acierta4(byte[] a, byte[] b){
        int cont=0;
        for (int i = 0; i < a.length; i++) {
            if(a[i]==b[i]){
                cont++;
            }

        }
        return cont==4;
    }
}
