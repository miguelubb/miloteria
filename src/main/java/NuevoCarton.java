import jdk.nashorn.internal.scripts.JO;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;

public class NuevoCarton extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField serie;
    private JTextField num5;
    private JTextField id;
    private JLabel qrImage;
    private JButton nuevoButton;
    private JButton validarButton;
    private JButton imprimirButton;
    private JTextField num1;
    private JTextField num2;
    private JTextField num4;
    private JTextField num3;
    private JTextField cantCartones;
    private JButton imprimeLoteButton;

    private Carton displayedCarton;

    public Carton getDisplayedCarton() {
        return displayedCarton;
    }

    public void setDisplayedCarton(Carton displayedCarton) {
        this.displayedCarton = displayedCarton;
        serie.setText(""+displayedCarton.getSerie());
        byte[] numeros=displayedCarton.getNum();
        num1.setText(numeros[0]+"");
        num2.setText(numeros[1]+"");
        num3.setText(numeros[2]+"");
        num4.setText(numeros[3]+"");
        num5.setText(numeros[4]+"");
        id.setText(displayedCarton.getId());
        BufferedImage img = null;
        try {
            img = ImageIO.read(new ByteArrayInputStream(displayedCarton.getQR()));

        } catch (IOException e) {
            e.printStackTrace();
        }
        qrImage.setIcon(new ImageIcon(img));

    }

    public NuevoCarton() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        nuevoButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            public void actionPerformed(ActionEvent e) {
                setDisplayedCarton(new Carton());
            }
        });
        imprimirButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                imprimirCarton();
            }
        });
        validarButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                //se actualiza el objeto Carton desplegado
                //según los datos del formulario
                byte[] numeros=displayedCarton.getNum();
                numeros[0]=Byte.parseByte(num1.getText());
                numeros[1]=Byte.parseByte(num2.getText());
                numeros[2]=Byte.parseByte(num3.getText());
                numeros[3]=Byte.parseByte(num4.getText());
                numeros[4]=Byte.parseByte(num5.getText());
                displayedCarton.setNum(numeros);
                displayedCarton.setId(id.getText());
                displayedCarton.setSerie(Integer.parseInt(serie.getText()));
                boolean cartonValido=Carton.validar(displayedCarton.getId(),displayedCarton.getNum(),displayedCarton.getSerie());
                if(cartonValido){
                    JOptionPane.showMessageDialog(null, "El cartón es válido");
                }else{
                    JOptionPane.showMessageDialog(null, "El cartón no es válido");
                }
            }
        });
        imprimeLoteButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                int num=Integer.parseInt(cantCartones.getText());
                Carton[] cartones=new Carton[num];
                for (int i = 0; i < cartones.length; i++) {
                    cartones[i]=new Carton();
                }
                try {
                    PrinterCarton.print(cartones);
                } catch (Exception e1) {
                    e1.printStackTrace();
                    JOptionPane.showMessageDialog(null,"No se pudo guardar los cartones");
                }
            }
        });
    }

    private void imprimirCarton() {

        try {
            PrinterCarton.print(displayedCarton);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "No se pudo generar PDF");
            e.printStackTrace();
        }


    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        NuevoCarton dialog = new NuevoCarton();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }



}
