import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.google.zxing.WriterException;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

public class ValidaCarton extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField serie;
    private JTextField numeros;
    private JTextField id;
    private JTextArea textQR;
    private JLabel QR;
    private JButton leerButton;
    private JPanel panelQR;

    private Executor executor = Executors.newSingleThreadExecutor();
    private AtomicBoolean initialized = new AtomicBoolean(false);
    private Webcam webcam = null;

    private ImageIcon qrIcon;
    public ValidaCarton() {
        loadIcon();
        webcam = Webcam.getDefault();
        webcam.setViewSize(webcam.getViewSizes()[0]);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });
        addWindowListener(new WindowAdapter() {
            /**
             * Invoked when a window has been opened.
             *
             * @param e
             */
            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                loadIcon();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        leerButton.addActionListener(new ActionListener()
        {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                executor.execute(new Runnable() {

                    @Override
                    public void run() {
                        leerQR();

                    }
                });

            }
        });

    }


    private void loadIcon(){

        ClassLoader classLoader=Thread.currentThread().getContextClassLoader();
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(classLoader.getResource("qrIcon.gif"));
            Image imageAux=new ImageIcon(bufferedImage).getImage();
            qrIcon=new ImageIcon(imageAux.getScaledInstance(160,120,Image.SCALE_SMOOTH));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void leerQR() {
        tomarFotoQR();

        Scanner sc=new Scanner(textQR.getText()).useDelimiter(":|\\n");
        sc.next();//me salto el título
        sc.next();//me salto la etiqueta Cartón
        int serieC=Integer.parseInt(sc.next().trim());
        serie.setText(""+serieC);
        sc.next();//me salto la etiqueta Número

        String num=sc.next().trim();
        numeros.setText(num);
        //me salto al propietario
        sc.next();sc.next();
        sc.next();//me salto la etiqueta hash
        id.setText(sc.next().trim());
        renderQR();
        onOK();
    }

    private void tomarFotoQR() {

        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        QR.setIcon(qrIcon);

        String msg=null;

        if(!webcam.isOpen())webcam.open();

        do {
            BufferedImage img = webcam.getImage();
            QR.setIcon(new ImageIcon(img));
            msg = QRGenerator.decodeQRCode(img);
        }while (msg==null);
        textQR.setText(msg);
        webcam.close();
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    private void renderQR(){
        byte[] qr={0};
        try {
            qr=QRGenerator.getQRCodeImage(textQR.getText(),150,150);
        } catch (WriterException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        BufferedImage img = null;
        try {
            img = ImageIO.read(new ByteArrayInputStream(qr));

        } catch (IOException e) {
            e.printStackTrace();
        }
        QR.setIcon(new ImageIcon(img));

    }
    private void onOK() {
        try {
            int serieC = Integer.parseInt(serie.getText().trim());
            if (Carton.validar(id.getText(), numeros.getText().trim(), serieC)) {
                JOptionPane.showMessageDialog(this, "cartón válido");
            } else {
                JOptionPane.showMessageDialog(this, "CARTÓN NO VALIDO", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }catch(NumberFormatException ex){
            JOptionPane.showMessageDialog(this,"Escanee o digite el cartón antes de validar");
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        showDialog();
        System.exit(0);
    }

    public static void showDialog() {
        ValidaCarton dialog = new ValidaCarton();
        dialog.pack();
        dialog.setVisible(true);
    }

}
