import com.google.zxing.WriterException;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

public class Carton implements Serializable {
    //
    private static final byte[] SALT={72, 81, -19, 59, -71, -11, -95, 76, 123, -43, 114, 30, 35, -52, -41, 45};
    private String id;//id seguridad
    private byte[] num;//números seleccionados
    private int serie;//número de serie


    private String propietario;//nombre del dueño del cartón

    public static boolean validar(String id, byte[] num, int serie){
        return validar(id,Arrays.toString(num),serie);
    }

    public static boolean validar(String id, String num, int serie){
        String pwd=serie + num;
        String idCalculado=SecureIDGenerator.get_SHA_256_SecurePassword(pwd,SALT);
        return id.equals(idCalculado);
    }


    public static boolean validar(Carton c){
        String pwd=c.getSerie() + Arrays.toString(c.getNum());
        String idCalculado=SecureIDGenerator.get_SHA_256_SecurePassword(pwd,SALT);
        return c.getId().equals(idCalculado);
    }
    private static byte rnd(){
        return (byte)(Math.random()*20+1);
    }


    private static int generarSerie(){
        return Sorteo.getInstance().generarSerie();
    }

    private static byte[] generaNumeros(){
        byte[] num=new byte[5];
        boolean noValido;
        do {
            noValido=false;
            num[0] = rnd();
            num[1] = rnd();
            num[2] = rnd();
            num[3] = rnd();
            num[4] = rnd();
            //busca repetidos, si hay no es válido
            Arrays.sort(num);
            for (int i = 1; i < num.length; i++) {
                if(num[i]==num[i-1]){
                    noValido=true;
                    break;
                }

            }
        }while(noValido);
        return num;
    }


    public Carton() {
        serie=generarSerie();
        num=generaNumeros();
        propietario="Al portador";
        String pwd=serie + Arrays.toString(num);
        id=SecureIDGenerator.get_SHA_256_SecurePassword(pwd,SALT);

    }

    public Carton(String prop) {
        serie=generarSerie();
        num=generaNumeros();
        propietario=prop;
        String pwd=serie + Arrays.toString(num);
        id=SecureIDGenerator.get_SHA_256_SecurePassword(pwd,SALT);

    }

    public Carton(byte[] num) {
        this.num = num;
        serie=generarSerie();
        propietario="Al portador";
        String pwd=serie + Arrays.toString(num);
        id=SecureIDGenerator.get_SHA_256_SecurePassword(pwd,SALT);

    }

    public Carton(byte[] num, String prop) {
        this.num = num;
        serie=generarSerie();
        propietario=prop;
        String pwd=serie + Arrays.toString(num);
        id=SecureIDGenerator.get_SHA_256_SecurePassword(pwd,SALT);

    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getNum() {
        return num;
    }

    public void setNum(byte[] num) {
        this.num = num;
    }

    public int getSerie() {
        return serie;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    @Override
    public String toString() {
        return  "SORTEO MILLONARIO\n" +
                "Cartón: "+serie+"\n"+
                "Números: " + Arrays.toString(num) +"\n"+
                "Propietario: "+ propietario+"\n"+
                "Hash: " + id;
    }

    public byte[] getQR(){
        try {
            return QRGenerator.getQRCodeImage(toString(),150,150);
        } catch (WriterException e) {
            e.printStackTrace();
            return new byte[]{0};
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[]{0};
        }
    }
}
