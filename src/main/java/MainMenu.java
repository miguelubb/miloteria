import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;

public class MainMenu extends JDialog {
    private JPanel contentPane;

    private JButton generarCartonesButton;
    private JButton validarUnCartónButton;
    private JButton buscarGanadoresButton;
    private JButton salirButton;

    public MainMenu() {
        setContentPane(contentPane);
        setModal(true);

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onSalir();
            }
        });

        // call onCancel() on ESCAPE
        /*contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        */

        generarCartonesButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                GeneraTalonarios.showDialog();
            }
        });
        validarUnCartónButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                ValidaCarton.showDialog();
            }
        });
        buscarGanadoresButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                BuscarGanadores.showDialog();
            }
        });
        salirButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                onSalir();
            }
        });
        salirButton.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                onSalir();
            }
        });
    }

    private void onSalir() {
        dispose();
        try {
            Sorteo.getInstance().toFile("Sorteo.obj");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        //carglo los talonarios en la instancia Sorteo
        try {
            Sorteo.getInstance().fromFile("Sorteo.obj");
        } catch (IOException e) {
            //si no se puede leer es porque no existe y no hay problemas si es la primera vez.
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        MainMenu dialog = new MainMenu();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
